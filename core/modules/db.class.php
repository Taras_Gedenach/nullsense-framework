<?php
class DB
{
    private $host;
    private $user;
    private $password;
    private $dbname;
    protected $pdo;

    public function fetchAssoc($query, $params)
    {
        $statement = $this->pdo->prepare($query);
        $statement->execute($params);
        $result = $statement->fetchAll(PDO::FETCH_ASSOC);
        
        if ($statement->errorCode() == 00000) {
            $statement = null;
            return $result;
        } else {
            $statement = null;
            return false;
        }
    }

    public function query($query, $params)
    {
        $statement = $this->pdo->prepare($query);
        $statement->execute($params);
        if ($statement->errorCode() == 00000) {
            $statement = null;
            return true;
        } else {
            //print_r($statement->errorInfo());
            $statement = null;
            return false;
        }
    }

    public function __construct()
    {
        $this->connect();
    }

    protected function connect()
    {
        $params = json_decode(file_get_contents(APP_BASEDIR."/config/mysql.json"), true);

        $this->user = $params["user"];
        $this->password = $params["password"];
        $this->host = $params["host"];
        $this->dbname = $params["db"];

        try {
            $mysqlPDOParams = "mysql:host={$this->host};dbname={$this->dbname}";
            $this->pdo = new pdo($mysqlPDOParams, $this->user, $this->password, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING));
        } catch (PDOException $error) {
            echo "ERROR: " . $error->getMessage() . "<br>";
            echo "Check your connection parameters in mysql.json";
            throw new Exception();
        }
    }

    public function __destruct()
    {
        $this->pdo = null;
    }
}
