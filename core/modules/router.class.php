<?php
class Router extends App
{
    /*
     * Router manages URL's for user friendly links
     */
    protected $routes;
    public $request;

    public function __construct()
    {
        parent::__construct();

        if (!empty($_GET['route'])) {
            $route = $_GET['route'];
            $path = explode('/', $route);

            //if last item of $path is empty, delete it
            if (empty($path[ count($path) -1 ])) {
            	$path = array_slice($path, 0, count($path) - 1);
            }
        } else {
            $path = null;
        }

        $this->configFile = 'routes';
        $this->routes = $this->getConfig();
        if (count($path) === 1) {
					//in case if url contains only lang

					//if $lang is in $this->availLocales
          foreach ($this->availLocales as $lang) {
						if ($path[0] == $lang) {
								header("Location: http://{$_SERVER['HTTP_HOST']}/{$path[0]}/login");
								die();
						}
					}
					//else
					header("Location: http://{$_SERVER['HTTP_HOST']}/".APP_DEFAULT_LOCALE."/{$path[0]}");
          die();
        } elseif (empty($path)) {
            header("Location: http://{$_SERVER['HTTP_HOST']}/".APP_DEFAULT_LOCALE."/login");
            die();
        } else {
						//in case if url contains NOT only lang
            $this->request['locale'] = APP_DEFAULT_LOCALE;
            foreach ($this->availLocales as $lang) {
                if ($path[0] == $lang) {
                    $this->request['locale'] = $lang;
                }
            }

            define('APP_LOCALE', $this->request['locale']);
            $this->request['page'] = $path[1];
        }

        $pageName = $this->request['page'];
        $pageGroup = $this->routes[$pageName]['group'];
        $userGroup = $this->auth->getUserGroup();

        if ($pageGroup != "any" && $userGroup != $pageGroup) {
						die("access denied");
            header("Location: http://{$_SERVER['HTTP_HOST']}/{$path[0]}/404");
        }
    }
    public function loadPage()
    {
        $pageComponents = $this->routes[$this->request['page']];
        $ctrlName = $pageComponents['controller'];
        //pass template name and page data as arguments
        $pagename = $this->request['page'];
        $pageCtrl = new $ctrlName($pagename);

        $pageCtrl->inject(array(
            "db" => &$this->db,
            "auth" => &$this->auth
        ));

        //fill errors array with framework errors
        $pageCtrl->setFrameworkErrors($this->getErrorMessages());
        $pageCtrl->bootstrap($this->request['page']);
    }
}
