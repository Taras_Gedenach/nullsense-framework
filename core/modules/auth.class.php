<?php
class Auth
{
	private $group;
	//private $accessToken;
	protected $db;
	private $availGroups;

	private function generatePasswordHash($email, $password)
	{
		return md5(substr($email, 0, strlen($email) - 3).$password);
	}

	private function generateAccessToken($password)
	{
		return hash("sha512", time().$password);
	}
	public function isCorrectPassword($email, $password) {
		$hash = $this->generatePasswordHash($email, $password);

		if(!empty($this->db->fetchAssoc(
			"SELECT * FROM users WHERE email = ? AND password = ?",
			array($email, $hash)
			)))
		{
			return true;
		}
		else return false;
	}
	public function isRegistered($email)
	{
		$args = func_get_args();
		if (func_num_args() > 1) {
			$login = $args[1];
			$query = "SELECT * FROM users WHERE email = ? OR login = ?";
			$result = $this->db->fetchAssoc($query, array($email, $login));
		}
		else {
			$query = "SELECT * FROM users WHERE email = ?";
			$result = $this->db->fetchAssoc($query, array($email));
		}

		if(!empty($result))
		{
			return true;
		}
		else return false;
	}

	public function getUserGroup()
	{
		if($this->isAuthorized())
			return "user";
		else return "visitor";
	}

	public function isAuthorized()
	{
		if( !empty($_COOKIE['login']) &&
			!empty($_COOKIE['access_token']) &&
			!empty($this->db->fetchAssoc(
				"SELECT * FROM users WHERE login = ? AND access_token = ?",
				array($_COOKIE['login'], $_COOKIE['access_token']) ))
		)
		{
			return true;
		}
		else return false;
	}

	public function logout()
	{
		$this->db->query(
			"UPDATE users SET access_token = '' WHERE login = ?",
			array($_COOKIE['login'])
		);
		setcookie("access_token", "", 0, "/");
		setcookie("login", "", 0, "/");
	}

	public function login($email, $password)
	{
		if ($this->isRegistered($email))
		{
			if (!$this->isCorrectPassword($email, $password)) {
				return array("error" => "Wrong password.");
			}
			$userInfo = $this->db->fetchAssoc(
				"SELECT login FROM users WHERE email = ?",
				array($email)
			);

			$accessToken = $this->generateAccessToken($password);
			$login = $userInfo[0]['login'];

			setcookie("login", $login, time()+365*24*60*60, '/');
			setcookie("access_token", $this->generateAccessToken($password), time()+365*24*60*60, '/');
			$hash = $this->generatePasswordHash($email, $password);
			$this->db->query(
				"UPDATE users SET access_token = ? WHERE password = ? AND email = ?",
				array($accessToken, $hash, $email)
			);

			header("Location: http://{$_SERVER["HTTP_HOST"]}/".APP_LOCALE."/user");
		}
		else
		{
			return array("error" => "User with this email is not registered.");
		}
	}

	public function signup($email, $password, $login)
	{
		if (!$this->isRegistered($email, $login))
		{
			//register user
			$hash = $this->generatePasswordHash($email, $password);

			$success = $this->db->query(
				"INSERT INTO users VALUES (0, ?, ?, ?, '', 0)",
				array($email, $login, $hash)
			);
			if($success)
			{
				$this->login($email, $password);
			}
		}
		else
		{
			return array("error"=> "User with this email or login is already registered");
		}
	}

	function __construct(&$dbObjectAddress)
	{
		$this->availGroups = array("visitor", "user");
		$this->db = $dbObjectAddress;
		if (empty($_COOKIE["login"]) || empty($_COOKIE["access_token"]))
		{
			$this->group = "visitor";
		}
	}
}
