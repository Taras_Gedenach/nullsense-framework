<?php
class DataFilter
{
	/*
	 * @params: string $direction, mixed $value [, string $varType]
	 * 		$direction - where data will be used:
	 * 			input:	in DB
	 *			output:	in Browser
	 * 		$value - variable value
	 * 		$varType - variable type (default: "string")
	 */
	static function get()
	{
		$args = func_get_args();
		
		if (func_num_args() == 3)
		{
			$direction = $args[0];
			$value = $args[1];
			$varType = $args[2];
		}
		elseif (func_num_args() == 2)
		{
			$direction = $args[0];
			$value = $args[1];
			$varType = "string";
		}
		else
		{
			throw new Exception("Function ".__FUNCTION__." takes 2 parameters maximum.");
		}
		
		if($direction == "output")
		{
			if ($varType = "string")
				return htmlspecialchars($value);
			elseif ($varType = "int")
				return (int)$value;
		}
		else if ($direction == "input")
		{
			if ($varType = "string")
				return $value;
			elseif ($varType = "int")
				return (int)$value;
			
		}
		
	}
}
