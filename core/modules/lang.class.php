<?php
trait Lang
{
    //file with translations for current page
    private $transPath;
    protected $translations;

    public function __construct()
    {
        $this->translations = array();
    }

    public function setCommonTranslations($path)
    {
        $this->translations = require APP_BASEDIR."/resources/".$path;
    }
    //@params: string $path - relative to resources dir
    public function setLocalePath($path)
    {
        $this->transPath = $path;
        $this->translations = array_merge(
            $this->translations,
            require APP_BASEDIR."/resources/".$this->transPath
        );
    }

    public function translate($text)
    {
        if (APP_LOCALE == APP_DEFAULT_LOCALE) {
            return $text;
        } else {
            if (empty($this->transPath)) {
                $this->addErrorMessage(__CLASS__.": transPath is empty");
            }
            if (!empty($this->translations[$text])) {
                return $this->translations[$text];
            } else {
                return "";
            }
        }
    }
}
