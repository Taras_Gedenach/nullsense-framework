<?php
class App
{
    protected $availLocales;
    protected $configFile;
    protected $auth;
    protected $errors;
    protected $db;

    public static function setParams()
    {
        $configFile = 'app';
        $config =  json_decode(file_get_contents(APP_BASEDIR."/config/{$configFile}.json"), true);
        define("APP_THEME", $config['theme']);
        define("APP_DEFAULT_LOCALE", $config['default locale']);
    }

    public function __construct()
    {
        $this->db = new DB();
        $this->auth = new Auth($this->db);
        $this->errors = array();
        $this->configFile = 'app';
        $config = $this->getConfig();
        $this->availLocales = $config['available locales'];
    }

    public static function addAutoloadThemePath()
    {
        set_include_path(get_include_path() . PATH_SEPARATOR . APP_BASEDIR."/resources/themes/".APP_THEME."/models/");
        set_include_path(get_include_path() . PATH_SEPARATOR . APP_BASEDIR."/resources/themes/".APP_THEME."/controllers/");
        spl_autoload_extensions('.class.php');
        spl_autoload_register();
    }

    public function getConfig()
    {
        return json_decode(file_get_contents(APP_BASEDIR."/config/{$this->configFile}.json"), true);
    }

    protected function getErrorMessages()
    {
        return $this->errors;
    }

    protected function addErrorMessage($text)
    {
        $this->errors[] = $text;
    }
}
