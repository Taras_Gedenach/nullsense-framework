# Installation

1. Import zorenodb.sql to your database.
2. Set mysql connection parameters in /config/mysql.json

# Notes

This framework requires mod_rewrite to be enabled.
