<?php
return [
	"is not present in translations file." => "відсутній в файлах перекладу.",
	"User with this email is not registered." => "Користувач з таким email не зареєстрований",
	"User with this email already registered" => "Користувач з таким email вже зареєстрований"
];
