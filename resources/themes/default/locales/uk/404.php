<?php
return [
	"Logout" => "Вийти",
    "Userpage" => "Сторінка користувача",
	"Page Not Found" => "Сторінку не знайдено",
	"Seem's like you misspelled in url or came here by broken URL.
Try again or visit <a href='/'>main page</a>"
	=>
	"Схоже, Ви зробили помилку в адресі на цю сторінку або 
потрапили сюди за зламаним посиланням. Спробуйте ще раз
або перейдіть на <a href='/'>головну сторінку</a>"
];
