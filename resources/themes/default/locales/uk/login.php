<?php
return [
    "Sign up" => "Реєстрація",
    "Log in" => "Вхід",
    "Password" => "Пароль",
    "Email" => "Email",
    "To see content on this site, you should log in first" => "Щоб переглянути вміст, Вам потрібно спочатку увійти на сайт"
];
