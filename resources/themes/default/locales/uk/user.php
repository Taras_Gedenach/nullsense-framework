<?php
return [
    "Sign up" => "Реєстрація",
    "Log in" => "Вхід",
    "Logout" => "Вийти",
    "Userpage" => "Сторінка користувача",
    "Password" => "Пароль",
    "Email" => "Email",
    "Confirm password" => "Підтвердіть пароль",
    "Upload your photo" => "Завантажте своє фото",
    "Welcome" => "Ласкаво просимо"
];
