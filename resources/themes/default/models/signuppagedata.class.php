<?php
class SignupPageData
{
	protected $email;
	protected $password;
	protected $passwordConfirm;
	protected $photo;
	protected $login;
	
	protected function getData()
	{
		$this->email = $_POST['email'];
		$this->password = $_POST['password'];
		$this->passwordConfirm = $_POST['passwordConfirm'];
		$this->photo = $_POST['photo'];
		$this->login = $_POST['login'];
	}
} 
