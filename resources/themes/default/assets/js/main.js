/*jshint enforceall: true, camelcase: true, undef: true, strict: true, browser: true, devel: false, jquery: true*/
$('document').ready(function () {
  'use strict';

	function FormValidator(formClass){
		this.errors = 0;
		this.formClass = formClass;
		this.clearWarnings = function (){
			this.errors = 0;
			$("form." + this.formClass + " div.warning").remove();
			$("form." + this.formClass + " input").removeClass("warning");
			$("form." + this.formClass + " .form-control-feedback").remove();
		};

		this.arePasswordsMatching = function (){
			if ($("form." + this.formClass + " input[name=password]").val() !== $("form." + this.formClass + " input[name=passwordConfirm]").val()) {
				$("form." + this.formClass + " input[name=passwordConfirm]").after(
					"<div class='warning'>This field must match the password</div>"
				);
				this.errors += 1;
			}
		};

		this.findEmptyFields = function (){
			$.each($("form." + this.formClass + " input"), function(key, elem){
				if(elem.required){
					if($(elem).val().trim() === ""){
						$(elem).addClass("warning");
						$(elem).parents(".form-group").append(
							"<div class='warning'>This field should not be empty</div>"
						);
						this.errors += 1;
					} else {
						$(elem).parents(".form-group").append(
							"<span class='glyphicon glyphicon-ok form-control-feedback'></span>"
						);
					}
				}
			});
		};

		this.checkEmail = function() {
			var regex = /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i;

			if($("form input[type=email]").val().match(regex) === null) {
				$(this).after(
					"<div class='warning'>Email shoul be valid! For example: user@host.com</div>"
				);
				this.errors += 1;
			}
		};
	}

    $("form.signup input[type=submit]").click(function(event){
		var validator = new FormValidator("signup");

        validator.clearWarnings();
        validator.findEmptyFields();
        validator.arePasswordsMatching();
        validator.checkEmail();

        if (validator.errors > 0) {
			event.preventDefault();
		}
    });

    $("form.login input[type=submit]").click(function(event){
		var validator = new FormValidator("login");

        validator.clearWarnings();
        validator.findEmptyFields();

        if (validator.errors > 0) {
			event.preventDefault();
		}
    });
});
