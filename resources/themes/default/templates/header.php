<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title><?php $this->trans($this->title);?></title>
	
	<link rel="stylesheet" type="text/css" href="/resources/bootstrap/css/bootstrap.min.css">
	
	<link rel="stylesheet" type="text/css" href="/resources/themes/default/assets/css/main.css">
	
	<script src="/resources/themes/default/assets/js/jquery-1.12.1.min.js"></script>
	<script src="/resources/bootstrap/js/bootstrap.min.js"></script>
    <script src="/resources/themes/default/assets/js/main.js"></script>
</head>
<body>
	<div class="container">
		<nav>
			<div class="container">
				<ul>
				<?php
				if($this->auth->isAuthorized())
				{
					?>
					<li>
						<a href="<?php echo "/".APP_LOCALE."/user" ?>">
							<?php $this->trans('Userpage');?>
						</a>
					</li>
					<li>
						<a href="<?php echo "/".APP_LOCALE."/login?logout" ?>">
							<?php $this->trans('Logout');?>
						</a>
					</li>
						
					<?php
				}
				?>
					
					<li class="lang-selector pull-right">
							<a href="/en/<?php echo $this->pageName; ?>">EN</a>
							/
							<a href="/uk/<?php echo $this->pageName; ?>">UA</a>
					</li>
				</ul>
			</div>
		</nav>
