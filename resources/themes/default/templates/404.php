<?php $this->insertTemplate("header");?>
        <div class="row">
            <div class="col-md-12">
				<h1><?php $this->trans("Page Not Found");?></h1>
				<div class="alert alert-danger">
					<?php $this->trans("Seem's like you misspelled in url or came here by broken URL.
Try again or visit <a href='/'>main page</a>");?>
				</div>
            </div>
        </div>
<?php $this->insertTemplate("footer");?>
