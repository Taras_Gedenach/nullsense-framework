<?php $this->insertTemplate("header");?>
        <div class="col-md-offset-3 col-md-6">
            <section class="row row-bg">
                <h2><?php $this->trans('Sign up');?></h2>
                <form method="POST" action="" class="signup">
					<div class="form-group has-feedback">
                        <label class="control-label"><?php $this->trans('Login');?>:</label>
                        <input type="text" class="form-control" name="login"  required>
                    </div>
                    <div class="form-group has-feedback">
                        <label class="control-label"><?php $this->trans('Email');?>:</label>
                        <input type="email" class="form-control" name="email" required>
                    </div>
                    <div class="form-group has-feedback">
                        <label class="control-label"><?php $this->trans('Password');?>:</label>
                        <input type="password" class="form-control" name="password"  required>
                        <span class="glyphicon form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <label class="control-label"><?php $this->trans('Confirm password');?>:</label>
                        <input type="password" class="form-control" name="passwordConfirm"  required>
                        <span class="glyphicon form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <label class="control-label"><?php $this->trans('Upload your photo');?>:</label>
                        <input type="file" class="form-control" name="photo">
                        <span class="glyphicon form-control-feedback"></span>
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-default" value="<?php $this->trans('Sign up');?>">
                        <a href="/<?php echo APP_LOCALE;?>/login"> | <?php $this->trans('Log in');?></a>
                    </div>
                </form>
            </section>
        </div>
<?php $this->insertTemplate("footer");?>
