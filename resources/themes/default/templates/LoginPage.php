<?php $this->insertTemplate("header");?>
        <div class="col-md-offset-3 col-md-6">
			
            <section class="row row-bg">
                <h2><?php $this->trans('Log in');?></h2>
                <p><?php $this->trans('To see content on this site, you should log in first');?></p>
                <form method="POST" action="" class="login">
                    <div class="form-group has-feedback">
                        <label class="control-label"><?php $this->trans('Email');?>:</label>
                        <input type="email" class="form-control" name="email" required>
                        <span class="glyphicon glyphicon-ok form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <label class="control-label"><?php $this->trans('Password');?>:</label>
                        <input type="password" class="form-control" name="password"  required>
                        <span class="glyphicon form-control-feedback"></span>
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-default" value="<?php $this->trans('Log in');?>">
                        <a href="/<?php echo APP_LOCALE;?>/signup"> | <?php $this->trans('Sign up');?></a>
                    </div>
                </form>
            </section>
        </div>
<?php $this->insertTemplate("footer");?>
