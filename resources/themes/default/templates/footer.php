	</div>
	<div class="container">
	<section class="row errors-notification">
		<?php
		if (!empty($this->errors))
		foreach ($this->errors as $error)
		{
		?>
			<div class="alert alert-danger text-danger">
				<b><?php $this->trans($error); ?></b>
			</div>
		<?php
		}
		?>
	</section>
	</div>
</body>
</html>
