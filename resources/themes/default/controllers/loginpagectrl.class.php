<?php
class LoginPageCtrl extends LoginPageData
{
	use MainCtrl;
	
	function __construct($pagename)
	{
		$this->pageName = $pagename;
		$this->loadParams();
	}
	
	protected function isValidFormData()
	{
		$isValid = true;
		
		if (empty($this->password) || empty($this->email))
		{
			$this->addErrorMessage("Field's shouldn't be empty");
		}
		return $isValid;
	}
	
	public function parseData()
	{
		if (isset($_GET['logout']))
		{
			$this->auth->logout();
		}
		if(!empty($_POST))
		{
			//fill LoginPageData with values from POST
			$this->getData();

			if($this->isValidFormData())
			{
				$result = $this->auth->login($this->email, $this->password);
				
				if(!empty($result["error"]))
				{
					$this->addErrorMessage($result["error"]);
				}
			}
		}
	}
	
}
