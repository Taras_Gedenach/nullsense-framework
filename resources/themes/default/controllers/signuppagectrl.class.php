<?php
class SignupPageCtrl extends SignupPageData
{
    use MainCtrl;

    public function __construct($pagename)
    {
        $this->pageName = $pagename;
        $this->loadParams();
    }

    public function parseData()
    {
        if (!empty($_POST)) {
            //fill SignupPageData with values
            $this->getData();
            if ($this->isValidFormData()) {
                $result = $this->auth->signup($this->email, $this->password, $this->login);

                if (!empty($result["error"])) {
                    $this->addErrorMessage($result["error"]);
                }
            }
        }
    }

    protected function isValidFormData()
    {
        $isValid = true;

        if (empty($this->password) ||
            empty($this->login) ||
            empty($this->email) ||
            empty($this->passwordConfirm)) {
            $this->addErrorMessage("Field's shouldn't be empty");
        }
        if ($this->password != $this->passwordConfirm) {
            $isValid = false;
            $this->addErrorMessage("Passwords doesn't match");
        }
        return $isValid;
    }
}
