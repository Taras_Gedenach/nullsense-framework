<?php
trait MainCtrl
{
    //Contains common methods for all of the pages
    use Lang;
    use CommonData;

    //Text post-translation
    private function trans($text)
    {
        if (!empty($text) && empty($this->translate($text))) {
            $this->addErrorMessage(__CLASS__.": '$text'" . $this->translate("is not present in translations file."));
        } else {
            echo $this->translate($text);
        }
    }

    //Add adresses of additional singleton classes into this object
    public function inject($classes)
    {
        foreach ($classes as $className => &$addr) {
            if (property_exists($this, $className)) {
                $this->$className = $addr;
            }
        }
        unset($addr);
    }

    //Fill errors array with existing framework errors
    public function setFrameworkErrors($errors)
    {
        foreach ($errors as $error) {
            $this->errors[] = $error;
        }
    }

    public function addErrorMessage($error)
    {
        $this->errors[] = $error;
    }

    //Load page parameters
    protected function loadParams()
    {
        $this->pagesConfig = json_decode(file_get_contents(APP_BASEDIR."/resources/pages/pages.json"), true);
        $this->template = $this->pagesConfig[$this->pageName]["template"];
        $this->title = $this->pagesConfig[$this->pageName]["title"];
    }

    protected function insertTemplate($templateName)
    {
        require APP_BASEDIR."/resources/themes/".APP_THEME."/templates/{$templateName}.php";
    }

    //Page boot process
    public function bootstrap()
    {
        $this->parseData();
        $this->render();
    }

    public function render()
    {
        $this->setCommonTranslations(
            "themes/".APP_THEME."/locales/".APP_LOCALE."/common.php"
        );

        //@params: string $path - relative to resources dir
        $this->setLocalePath(
            "themes/".APP_THEME."/locales/".APP_LOCALE."/{$this->pageName}.php"
        );

        $this->insertTemplate($this->template);
    }
}
